import React, { Component } from 'react';
import './App.css';

import { Route, Switch } from 'react-router-dom';

import About from './About';
import Contact from './Contact';
import Profile from './Profile';
import Homepage from './Homepage';
import Header from './Header';
import NoMatch from './NoMatch';

class App extends Component {
  render() {
    return (
      <div>
      	<Header />
      	<div className="page-container">
	      	<Switch>
	      		<Route exact path="/" component={Homepage} />
	      		<Route exact path="/about" component={About} />
	      		<Route exact path="/Contact" component={Contact} />
	      		<Route path="/profile/:name" component={Profile} />
	      		<Route component={NoMatch} />
	      	</Switch>
	    </div>
      </div>
    );
  }
}

export default App;
