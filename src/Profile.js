import React, { Component } from 'react';

class Profile extends Component {
  render() {

  	const name = this.props.match.params.name;

    return (
      <div>
      	<h1>Profile {name}</h1>
      </div>
    );
  }
}

export default Profile;
