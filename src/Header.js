import React, { Component } from 'react';
import './Header.css';

import { Link } from 'react-router-dom';

class Header extends Component {
  render() {
    return (
      <header>
      	<nav>
      		<ul>
      			<li><Link to="/">Homepage</Link></li>
      			<li><Link to="/about">About</Link></li>
      			<li><Link to="/contact">Contact</Link></li>
      			<li><Link to="/profile/fred">Profile: Fred</Link></li>
      			<li><Link to="/profile/bob">Profile: Bob</Link></li>
      			<li><a href="http://google.com" rel="noopener noreferrer" target="_blank">External</a></li>
      		</ul>
      	</nav>
      </header>
    );
  }
}

export default Header;
